import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  public items: any;

  constructor(public navCtrl: NavController, public http: HttpClient){
    this.loadData();
  }

  loadData() {
    let data: Observable<any>;
    data = this.http.get('https://jsonplaceholder.typicode.com/comments');
    data.subscribe(result => {
      this.items = result;
    })
  }
}



  /* ASSIGNMENT - 1
  id: Int32Array;
  username: string;
  password: string;

  text = "Click button to check your inputs";
  onChangeText(){
    this.text = "Your ID: " + this.id + " Your Username: " + this.username + " Your Password: " + this.password;
  }
  */
